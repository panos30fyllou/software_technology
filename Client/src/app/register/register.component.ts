import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  role: string = "";
  constructor(private router: Router) {
   }

  ngOnInit(): void {
    if(this.router.url == "/register/Seller")
      this.role = "Seller";
    else if(this.router.url == "/register/Customer")
      this.role = "Customer";
  }

}
