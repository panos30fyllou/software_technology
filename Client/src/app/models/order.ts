import { OrderProduct } from "./order-product";

export class Order {
    id? : number; 
    userId? : number;
    storeId? : number;
    orderDate?: Date;
    estimatedDate?: Date;
    orderProducts?:OrderProduct[];

}
