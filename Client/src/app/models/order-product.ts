export class OrderProduct {
    orderId? : number;
    productid?: number;
    quantity: number;

    constructor(){
        this.quantity = 0;
    }
}
