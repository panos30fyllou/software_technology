import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MouseEvent } from '@agm/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  zoom= 8 ;
  role: string = "";
  lat = 37.974577;
  lng = 23.735422;
  markers:marker[]=[];

  constructor(private router: Router) { }

  ngOnInit(): void {
    if(this.router.url == "/register/Seller")
    this.role = "Seller";
  else if(this.router.url == "/register/Customer")
    this.role = "Customer";
  }
  clickedMarker(label: string, index: number) {
    console.log(`clicked the marker: ${label || index}`)
  }
  
  mapClicked($event: MouseEvent) {
    this.markers = [{
      lat: $event.coords.lat,
      lng: $event.coords.lng,
      draggable: true
    }];
  }
  
  markerDragEnd(m: marker, $event: MouseEvent) {
    console.log('dragEnd', m, $event);
  }

  submitLocation(){
    console.log(this.markers[0].lat,this.markers[0].lng);
  }

}

interface marker {
	lat: number;
	lng: number;

	draggable: boolean;

}
