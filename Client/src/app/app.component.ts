import { Component } from '@angular/core';
import { AuthService } from './services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  cartTooltipText = "Cart";
  userTooltipText = "Profile";
  title_icon = "shopping_cart";
  cartPath = '/cart';
  userPath = '/profile';

  opened = false;
  isLoggedIn?: boolean = false;
  constructor(private auth: AuthService) { }

  ngOnInit(): void {
    console.log("Checking login status")
    this.auth.checkLoginStatus
      .subscribe(result => (this.isLoggedIn = result));
    console.log("Login status: " + this.isLoggedIn);
  }
}
