import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Subscription } from 'rxjs/internal/Subscription';
import { Role, RoleMapping } from '../interfaces/role';
import { User } from '../interfaces/user';
import { AuthService } from '../services/auth.service';
import { LoginService } from '../services/login.service';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {
  loginSubscription: Subscription = new Subscription();
  currentUser?: Observable<User>;
  message?: string;
  public roles;
  selectedRole: any = Role.Customer;
  user?:any;
  constructor(private loginService: LoginService, private router: Router, private auth: AuthService, private snackBar: MatSnackBar) {
    this.roles = RoleMapping;
  }

  ngOnInit(): void {
    localStorage.setItem("profileType", "Customer");  
  }

  onSubmit(data: any): void {
    //console.log(this.loginService.login(data))
    //this.loginSubscription = this.loginService.login(data).subscribe((data) => (this.auth.changeLoginStatusTrue()));
    this.auth.changeLoginStatusTrue()
    this.router.navigate(['/home']);
    console.log(this.loginSubscription);
  
    
    localStorage.setItem("userId", "0");
    //(error) => this.handleError(error));
  }

  goToRegister(): void {
    console.log(this.selectedRole.value);
    this.router.navigate(["/register/" + this.roles[this.selectedRole].type]);
  }

  handleError(error: HttpErrorResponse) {
    if (error instanceof ErrorEvent)  this.snackBar.open("A Client-Side error occured!", "OK", { duration: 5000 });
    else                              this.snackBar.open(error.error, "OK", { duration: 5000 })
  }
}
