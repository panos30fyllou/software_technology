import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Order } from '../models/order';
import { OrderProduct } from '../models/order-product';

@Injectable({
  providedIn: 'root'
})
export class OrdersService {
  url: string = "";
  orderProducts:OrderProduct[]=[];
  constructor(private http:HttpClient) { }

  getAllStore(storeId: number): Observable<Order[]>{
    let params = new HttpParams().set("StoreId", storeId);
    this.url = environment.serverUrl + "/UserInterface/GetStoreOrders";
    return this.http.get<Order[]>(this.url, {params: params});
  }

  getAllClient(userId: number): Observable<Order[]>{
    let params = new HttpParams().set("StoreId", userId);
    this.url = environment.serverUrl + "/UserInterface/GetClientOrders";
    return this.http.get<Order[]>(this.url, {params: params});
  }

  add(order:Order){
    this.url = environment.serverUrl + "/UserInterface/AddOrder";
    return this.http.post(this.url, order);
  }

  setOrderProducts(orderProducts: OrderProduct[]){
    this.orderProducts = orderProducts;
  }
  
  getOrderProducts(){
    return this.orderProducts;
  }

}
