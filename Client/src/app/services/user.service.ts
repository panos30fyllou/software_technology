import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { environment } from 'src/environments/environment';
import { User } from '../interfaces/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  url: string = "";
  
  constructor(private http: HttpClient) { }

  selectById(id: number): Observable<User> {
    let params = new HttpParams().set("id", id);
    this.url = environment.serverUrl + "/User/SelectById"
    return this.http.get<User>(this.url, { params: params })
  }

  update(user: User){
    this.url = environment.serverUrl + "/User/Update";
    let params = new HttpParams().set("id", user.id);
    return this.http.put(this.url, user , {params:params});
  }
}
