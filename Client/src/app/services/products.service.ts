import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Product } from '../models/product';
import { ProductStock } from '../models/product-stock';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {
  url: string = "";

  constructor(private http:HttpClient) { }

  getAll(storeId:number):Observable<ProductStock[]>{
    let params = new HttpParams().set("StoreId", storeId);
    this.url = environment.serverUrl + "/UserInterface/GetProducts";
    return this.http.get<ProductStock[]>(this.url, {params: params});
  }
 
}
