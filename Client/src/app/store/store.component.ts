import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { Orders } from '../interfaces/orders';
import { Order } from '../models/order';
import { ProductStock } from '../models/product-stock';

const ORDERSDATA: Order[] = [
  {
    id: 1,
    userId: 1,
    storeId: 1,
    orderDate: new Date('12-12-2020'),
    estimatedDate: new Date('12-12-2020'),
    orderProducts: []
  },
  {
    id: 2,
    userId: 2,
    storeId: 2,
    orderDate: new Date('12-12-2020'),
    estimatedDate: new Date('12-12-2020'),
    orderProducts: []
  }
]

const STOCKDATA: ProductStock[] = [
  { product: { id: 1, name: "Swvrako", price: 10, description: "Foremeno", image: "" }, quantity: 5 },
  { product: { id: 2, name: "Swvrakofanela", price: 15, description: "Foremeno kai vrwmiko", image: "" }, quantity: 3 }
]

@Component({
  selector: 'app-store',
  templateUrl: './store.component.html',
  styleUrls: ['./store.component.css']
})
export class StoreComponent implements OnInit {
  orders: MatTableDataSource<Orders> = new MatTableDataSource();
  role: string = "";
  profileType: string = "";
  displayedColumns: string[] = [];
  stockDataSource = STOCKDATA;
  ordersDataSource = ORDERSDATA;
  constructor(private router: Router) { }
  page: string = "";

  ngOnInit(): void {
    if (this.router.url == "/store/orders") {
      this.page = "orders";
      this.displayedColumns = ['Id', 'StoreId', 'OrderDate', 'EstimatedDate'];
    }
    else if (this.router.url == "/store/stock") {
      this.page = "stock"
      this.displayedColumns = ['name', 'price', 'description', 'quantity', 'cart'];
    }

    console.log(this.page);
  }



}
