import { Component, Input, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  @Input()
  title?: string;
  @Input()
  cartPath?: string;
  @Input()
  userPath?: string;
  @Input()
  icon?: string;
  @Input()
  userTooltipText?: string;
  @Input()
  cartTooltipText?: string;
  @Input()
  userId?: number;
  
  username : string | null  = '';
  isLoggedIn?: boolean;
  constructor(private auth: AuthService) { }

  ngOnInit(): void {
    this.auth.checkLoginStatus
      .subscribe(result => (this.isLoggedIn = result, console.log(result)));
  }
}