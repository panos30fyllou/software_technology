import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';


@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})


export class NavComponent implements OnInit {
  @Input()
  profileType: string = "";
  opened = false;
  isLoggedIn?: boolean = false;
  role: string = "";
  constructor(private auth: AuthService, private router: Router) { }

  ngOnInit(): void {
  //   if(this.router.url == "/home/Seller")
  //   this.role = "Seller";
  // else if(this.router.url == "/home/Customer")
  //   this.role = "Customer";
    console.log("Checking login status");
    this.auth.checkLoginStatus
      .subscribe(result => (this.isLoggedIn = result, console.log(result)));
    if (localStorage.getItem("profileType") == "Seller")
      this.profileType = "Seller"
    else if (localStorage.getItem("profileType") == "Customer")
      this.profileType = "Customer"
    console.log(this.profileType);
  }

  logOut(): void {
    this.auth.logOut();
  }
}
