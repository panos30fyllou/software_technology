import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { Orders } from '../interfaces/orders';
import { Router } from '@angular/router';
import { Order } from '../models/order';
import { formatDate } from "@angular/common";
import { OrdersService } from '../services/orders.service';

export class PeriodicElement {
  name?: string;
  position?: number;
  weight?: number;
  symbol?: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H'},
  {position: 2, name: 'Helium', weight: 4.0026, symbol: 'He'},
  {position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li'},
  {position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be'},
  {position: 5, name: 'Boron', weight: 10.811, symbol: 'B'},
  {position: 6, name: 'Carbon', weight: 12.0107, symbol: 'C'},
  {position: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N'},
  {position: 8, name: 'Oxygen', weight: 15.9994, symbol: 'O'},
  {position: 9, name: 'Fluorine', weight: 18.9984, symbol: 'F'},
  {position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne'},
];

const DATA: Order[] = [
  {
    id: 1,
    userId:1,
    storeId:1,
    orderDate:new Date('12-12-2020'),
    estimatedDate:new Date('12-12-2020'),
    orderProducts:[]
  },
  {
    id: 2,
    userId:2,
    storeId:2,
    orderDate:new Date('12-12-2020'),
    estimatedDate:new Date('12-12-2020'),
    orderProducts:[]
  }
]

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})

export class OrdersComponent implements OnInit {
  orders: MatTableDataSource<Orders> = new MatTableDataSource() ;
  role: string = "";
  profileType: string = "";
  displayedColumns: string[] = ['Id', 'StoreId', 'OrderDate', 'EstimatedDate'];
  dataSource = DATA;

  constructor(private router: Router, private ordersService: OrdersService) { }

  ngOnInit(): void {
    if (localStorage.getItem("profileType") == "Seller")
      this.profileType = "Seller"
    else if (localStorage.getItem("profileType") == "Customer")
      this.profileType = "Customer"
    console.log(this.profileType);
  }


  // delete(order: Orders) {
  //   this.orderService.remove(this.orders.orderId).subscribe((success) => (this.snackBar.open("Customer successfully deleted","OK",{duration:5000}), this.selectAll()), (error) => this.handleError(error))
  // }
}
