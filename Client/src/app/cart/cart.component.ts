import { Component, OnInit } from '@angular/core';
import { Order } from '../models/order';
import { OrderProduct } from '../models/order-product';
import { OrdersService } from '../services/orders.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  displayedColumns: string[] = ['name','price','quantity','remove'];
  dataSource:OrderProduct[]=[];
  order:Order = new Order();
  constructor(private orderService:OrdersService) { }
  

  ngOnInit(): void {
    this.dataSource = this.orderService.getOrderProducts();

  }
   submitOrder(){
     this.order.storeId = 1;
     this.order.userId = 1;
     this.order.orderProducts=this.dataSource;
     this.orderService.add(this.order);
   }

}
