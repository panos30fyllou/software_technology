export enum Role {
    Customer,
    Seller,
    Admin
}

export const RoleMapping = [
    { value: Role.Customer, type: 'Customer' },
    { value: Role.Seller, type: 'Seller' },
];