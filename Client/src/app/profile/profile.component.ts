import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from '../interfaces/user';
import { UserService } from '../services/user.service';
import { Role, RoleMapping } from '../interfaces/role';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  roleMapping;
  message?: string;
  operation?: string;
  showDetails: boolean = false;
  user: User = {
    id: 0,
    username: "",
    password: "",
    role: 0
  }
  profileType?: string = "";
  constructor(private route: ActivatedRoute, private router: Router, private userService: UserService, private snackBar: MatSnackBar,) {
    this.roleMapping = RoleMapping;

  }

  ngOnInit(): void {
    const id = Number(this.route.snapshot.paramMap.get('id'));
    this.selectById(id);
    if (localStorage.getItem("profileType") == "Seller")        this.profileType = "Seller"
    else if (localStorage.getItem("profileType") == "Customer") this.profileType = "Customer"

    console.log("Hi" + this.profileType);
  }

  onSubmit() {
    this.userService.update(this.user)
      .subscribe((success) => (this.router.navigate(['/home']),
        this.snackBar.open("User succefully updated!", "OK", { duration: 5000 })),
        (error) => this.handleError(error));
  }

  selectById(id: number) {
    this.userService.selectById(id).subscribe((user) => this.user = user);
    console.log(this.roleMapping[this.user.role].type);
    this.profileType = this.roleMapping[this.user.role].type;
    console.log(this.profileType);
  }

  handleError(error: HttpErrorResponse) {
    if (error instanceof ErrorEvent) this.snackBar.open("A Client-Side error occured!", "OK", { duration: 5000 });
    else this.snackBar.open(error.error, "OK", { duration: 5000 });
  }


}
