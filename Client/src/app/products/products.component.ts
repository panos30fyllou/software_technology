import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { Products } from '../interfaces/products';
import { OrderProduct } from '../models/order-product';
import { Product } from '../models/product';
import { ProductStock } from '../models/product-stock';
import { OrdersService } from '../services/orders.service';
import { ProductsService } from '../services/products.service';

const DATA: ProductStock[] = [
  {product: {id:1,name:"Swvrako",price:10,description:"Foremeno",image:""},quantity: 5},
  {product: {id:2,name:"Swvrakofanela",price:15,description:"Foremeno kai vrwmiko",image:""},quantity: 3}
]
@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  role: string = "";
  profileType: string = "";
  displayedColumns: string[] = ['name', 'price', 'description', 'quantity','cart'];
  dataSource = DATA;
  orderProducts:OrderProduct[]=[];
  constructor(private router: Router, private orderService:OrdersService, private productService:ProductsService) { }

  ngOnInit(): void {
    if (localStorage.getItem("profileType") == "Seller")
    this.profileType = "Seller"
  else if (localStorage.getItem("profileType") == "Customer")
    this.profileType = "Customer"
  console.log(this.profileType);
  }

  addToCart(productId:number){
    let flag = false;
    this.orderProducts.forEach(item=>
      {
        if (item.productid==productId){
          item.quantity += 1;
          flag = true
        } 
      }
      )
      if (!flag){
        let product = new OrderProduct()
        product.productid = productId;
        product.quantity = 1;
        this.orderProducts.push(product)
      }
      console.log(this.orderProducts);
      
  }

  proceedToCart(){
    this.orderService.setOrderProducts(this.orderProducts);
    this.router.navigate(["/cart"]);
  }

}
